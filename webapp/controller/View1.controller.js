sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/Text"
], function (Controller, Text) {
	"use strict";

	return Controller.extend("Tutoriales.ContextBinding.controller.View1", {
		onInit: function () {
			var oVerticalLayout = this.getView().byId('vLayout');
			oVerticalLayout.bindElement("/company");
			oVerticalLayout.addContent(new Text({
				text: "{name}"
			}));
			oVerticalLayout.addContent(new Text({
				text: "{city}"
			}));
			oVerticalLayout.addContent(new Text({
				text: "{county}"
			}));

			//
		/*	var oInputName = this.getView().byId('inputName');

			oInputName.bindElement("oCompanies/companies/0");

			var oList = this.byId("companyList");
			oList.bindElement("oRegion/regions/0");*/

		}

	});
});